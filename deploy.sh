#!/bin/bash

# Assumes that JFROG_DEPLOYER_PASSWORD and CIRCLE_BUILD_NUM are set

DOCKER_REPO=quadanalytix-docker.jfrog.io
DOCKER_IMAGE=quad-iamp-automation-setup

docker build -t $DOCKER_IMAGE .

docker login $DOCKER_REPO -u deployer -p $JFROG_DEPLOYER_PASSWORD

docker tag $DOCKER_IMAGE $DOCKER_REPO/$DOCKER_IMAGE:`date +%Y.%m`

docker push $DOCKER_REPO/$DOCKER_IMAGE
