# Use lambdalinux/baseimage-amzn as base image.
# See https://hub.docker.com/r/lambdalinux/baseimage-amzn/tags/ for a list of version numbers.

FROM quadanalytix-docker.jfrog.io/quad-ds-python:latest
# Use baseimage-amzn's init system
CMD ["/sbin/my_init"]

# Update RPM packages
ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
ENV AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
RUN yum update

#Copy repos files for mongodb,cdh and ha
COPY yum.repos.d/* /etc/yum.repos.d/

RUN  yum clean all

# Install software
RUN yum -y install epel-release
RUN yum install cowsay fortune-mod rpm-build rpm-sign gcc-c++.noarch postgresql-server postgresql-contrib postgresql92-devel.x86_64 vi vim which git apache-maven jdk1.8.0_91.x86_64
#RUN yum install python27
RUN yum install wget
#RUN yum install python-pip python-wheel
RUN yum install gcc-c++
#RUN yum install python-devel
RUN pip install jaydebeapi
RUN pip install psycopg2
RUN pip install pybuilder
RUN pip install boto3
RUN pip install requests
#RUN pip install git+https://github.com/quadanalytix/dataduct

#Tweak jaydebeapi code to convert java long into python compatible type
RUN sed -i -e "s/_DEFAULT_CONVERTERS = {/_DEFAULT_CONVERTERS = {\n 'BIGINT': _java_to_py('longValue'),/g" /usr/local/lib64/python2.7/site-packages/jaydebeapi/__init__.py
RUN sed -i "s/STRING = DBAPITypeObject('CHAR', 'NCHAR', 'NVARCHAR', 'VARCHAR', 'OTHER')/STRING = DBAPITypeObject('ARRAY','CHAR', 'NCHAR', 'NVARCHAR', 'VARCHAR', 'OTHER')/" /usr/local/lib64/python2.7/site-packages/jaydebeapi/__init__.py

#Install CDH 5.7.0 ,zookeeper,hbase,mongoDB
RUN wget https://archive.cloudera.com/cdh5/one-click-install/redhat/6/x86_64/cloudera-cdh-5-0.x86_64.rpm
RUN yum --nogpgcheck localinstall cloudera-cdh-5-0.x86_64.rpm
RUN rpm --import https://archive.cloudera.com/cdh5/redhat/6/x86_64/cdh/RPM-GPG-KEY-cloudera
RUN yum install hadoop-conf-pseudo

RUN yum install zookeeper-server
RUN service zookeeper-server init
RUN yum install hbase
RUN yum install hbase-master.x86_64
RUN yum install hbase-regionserver.x86_64
RUN yum install -y mongodb-org-server
RUN yum install -y mongodb-org-shell
RUN yum install -y mongodb-org-tools

RUN pip install awscli --ignore-installed six
RUN chown -R ll-user:ll-user /home/ll-user

RUN echo 'export LC_ALL=en_US.UTF-8' >> ~/.bashrc
RUN echo 'export LANG=en_US.UTF-8' >> ~/.bashrc
RUN echo 'export PYTHONPATH=$PYTHONPATH:/usr/lib64/python2.7/site-packages/' >> ~/.bashrc

#Setup and initialize hadoop
RUN sed -i '/<configuration>/a <property>\n<name>hbase.cluster.distributed</name>\n<value>true</value>\n</property>\n<property>\n<name>hbase.rootdir</name>\n<value>hdfs://localhost:8020/hbase</value>\n</property>\n' /etc/hbase/conf/hbase-site.xml

RUN fortune -s | cowsay

# Add maven settings.xml file to the image
COPY .m2/settings.xml /home/ll-user/.m2/

#Enable quad-dev repo

RUN sed -i 's/enabled=0/enabled=1/' /etc/yum.repos.d/quad-dev.repo
RUN  yum clean all
WORKDIR /home/ll-user
